import React from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import { Container } from 'react-bootstrap';

//For routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// The Router(BrowserRouter) component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.

// The Routes(before it calls Switch) declares the Route we can go to. For example, when we want to visit the courses page only.

function App() {
  return (
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
          < Route path="/" element={ <Home /> }/>
          < Route path="/courses" element={ <Courses /> }/>
          < Route path="/login" element={ <Login /> }/>
          < Route path="/register" element={ <Register /> }/>        
          < Route path="/Logout" element={ <Logout /> }/>        
        </Routes>
      </Container>
    </Router>
      );
}

export default App;

