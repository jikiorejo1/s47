import React, { useEffect, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'



export default function Register(){
    
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(true);

    useEffect(() => {
        if((email!== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else {
            setIsActive(false);
        }
    }, [email, password1, password2])

    function registerUser(e) {
        e.preventDefault();

        setEmail('');
        setPassword1('');
        setPassword2('');

        Swal.fire({
            title: "Wow",
            icon: "success",
            text: "Thank you for Registering!"
        });
    }

    return (
        <Form onSubmit={(e) => registerUser(e)}>
            <h1>REGISTER</h1>
            <Form.Group>
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                    type='email'
                    placeholder='Enter Email'
                    required value={email}
                    onChange={e => setEmail(e.target.value)} />
                    <Form.Text className='text-muted'>
                        We'll Never share your email EVER!
                    </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type='password'
                    placeholder='Enter Password'
                    required value={password1} onChange={e => setPassword1(e.target.value)} />
            </Form.Group>

            <Form.Group>
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                    type='password'
                    placeholder='Verify Password'
                    required value={password2} onChange={e => setPassword2(e.target.value)} />
            </Form.Group>

            {isActive ?
                <Button variant='primary' type='submit' className='mt-3'>Confirm</Button>

                :

                <Button variant='primary' type='submit' className='mt-3' disabled>Confirm</Button>
            }
        </Form>
    )
}