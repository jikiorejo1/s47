import React, { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';


export default function CourseCard({courseProp}) {

	//check to see if the data was successfully passed
	// console.log(props)
	// console.log(typeof props)


	//Deconstruct the courseProp into their own variables
	const { name, description, price } = courseProp;

	//const [currentValue(getter), updatedValue(setter)] = useState(InitialGetterValue)

	const [count, setCount] = useState(0);
	const [seatCount, seatSetCount] = useState(30);

	const [isOpen, setIsOpen] = useState(true);

	function enroll(){
		if(count < 30 && seatCount > 0){
			setCount(count + 1);
			seatSetCount(seatCount - 1)
			console.log("Enrollees: " + count);
			console.log("Seat: " + seatCount);
		}else{
			alert("No More Seats");
		}
	}

	useEffect(() => {
		if(seatCount === 0){
			setIsOpen(false);
		}
	}, [seatCount])

	return(

		<Card className="m-3">
			<Card.Body>
				<Card.Title> { name } </Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text> { description } </Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php { price }</Card.Text>
				<Card.Text>Enrollees: { count } </Card.Text>
				<Card.Text>Seats: { seatCount } </Card.Text>
				
				{isOpen ? 
					<Button variant="primary" onClick={enroll}>Enroll</Button>
					:
					<Button variant="primary" disabled>Enroll</Button>
				}
			</Card.Body>
		</Card>

		)
}